### 1.下载依赖
```sh
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple django
```
### 2.启动项目
```shell
 python manage.py runserver     
```
### 3.启动网页
<a href="http://127.0.0.1:8000/book">点击这个访问项目</a></br>
### 项目不依赖数据库 基于内存数据模拟

# 作者声明

## 介绍

我，[miku]，是本作品的作者。本文档是关于我的作者身份、内容的原创性以及我的权利的官方声明。

## 联系信息

如果您有任何问题或需要进一步的信息，可以通过[libo.manger@gmail.com]与我联系。

感谢您尊重作者的权利和创造性努力。
## 用途

此项目用于python期中测验，如果您有任何问题或需要进一步的信息，可以通过[libo.manger@gmail.com]与我联系。

感谢您尊重作者的权利和创造性努力。

[miku]
[QQ1492881978]
[2023-10-29]
